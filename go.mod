module okocam

go 1.20

require (
	gocv.io/x/gocv v0.35.0
	okotek.ai/x/okofrm v1.1.1
)

require (
	gopkg.in/yaml.v3 v3.0.1 // indirect
	okotek.ai/x/okocfg v0.0.0-00010101000000-000000000000 // indirect
)

replace okotek.ai/x/okocfg => ../okocfg

replace okotek.ai/x/okofrm => ../okofrm

package main

import (
	"fmt"
	"os"
	"sync"
	"time"

	"gocv.io/x/gocv"
	okofrm "okotek.ai/x/okofrm"
)

func main() {

	var tmpMat gocv.Mat = gocv.NewMat()
	cam, camerr := gocv.VideoCaptureDevice(0)
	if camerr != nil {
		fmt.Println("FAIL: ", camerr)
		os.Exit(1)
	}

	uName := "testName"
	uPass := "testPass"

	sendoff := make(chan gocv.Mat)

	go mbHandler(sendoff, uName, uPass)

	go func() {
		for {

			cam.Read(&tmpMat)
			sendoff <- tmpMat

		}
	}()

	//time.Sleep(1 * time.Second)

	//for tmpMat := range sendoff {
	//	fmt.Println("Chans: ", tmpMat.Channels())
	//}

	var stopper sync.WaitGroup
	stopper.Add(1)
	stopper.Wait()
}

func mbHandler(inChan chan gocv.Mat, uName string, uPass string) {

	//func makeMatBox(mat gocv.Mat, CapTime uint64, UName string, UPass string) MatBox {

	for tmpMat := range inChan {
		go func(tmpMat gocv.Mat, uName string, uPass string) {
			sendoff := okofrm.MakeMatBox(tmpMat, uint64(time.Now().UnixNano()), uName, uPass)
			sendoff.MatBoxSendoff("localhost:8080")
		}(tmpMat, uName, uPass)
	}
}
